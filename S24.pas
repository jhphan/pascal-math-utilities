Program S24;

Uses 
   Crt;

Var
   Temp,
   A, B, Count, Found : Integer;
   OVar               : Array[1 .. 4] of Integer; {Original numbers}
   IVar               : Array[1 .. 4] of Integer; {Variable set 1}
   TVar               : Array[1 .. 4] of Integer; {Variable set 2}
   YesNo              : Char;

{------------------------------------------------------------------------}

Function Operate (Num : Integer) : Integer;

Var
   A, B, C, D : Integer;
   OpVar      : Array[1 .. 4] Of Integer;
   Op         : Array[1 .. 3] Of String;
   Group      : String;

{SUB PROC OPERATE : CHECK------------------------------------------------}

Function Check(Var1, Var2, Var3, Var4 : Integer) : Integer;

{
        Combination.Array Number (grouping)
        1.1 xxxx: 2.2(xx)xx: 3.3 x(xx)x: 4.4 xx(xx)
        5.(xx)(xx): 6.5 (xxx)x: 7.6 x(xxx)
}

Var
   Count  : Integer;
   Grp    : Array[2 .. 6] of Real;
   Final,
   Answer : Real;

{SUB PROC OPERATE CHECK : DoOp--------------------------------------------}

Procedure DoOp (Var Answer, Final : Real; Num : Integer;
               O : String; NNum : Real);

Var
   Temp : Real;

Begin
   If Num = 1
      Then Temp := Answer
      Else Temp := Final;

   If O = ' + ' Then
      Temp := Temp + NNum
   Else If O = ' - ' Then
      Temp := Temp - NNum
   Else If O = ' * ' Then
      Temp := Temp * NNum
   Else If O = ' / ' Then
    Begin
      If NNum = 0 Then Exit;
      Temp := Temp / NNum;
    End;

   If Num = 1
      Then Answer := Temp
      Else Final := Temp;
End;

{SUB PROC Operate CHECK : DoOp END---------------------------------------}

Begin
   Answer := 0;
   DoOp(Answer, Final, 1, ' + ', OpVar[1]);
   If Var1 = 1 Then
   Begin
      Op[1] := ' + ';
      DoOp(Answer, Final, 1, Op[1], OpVar[2]);
   End
   Else If Var1 = 2 Then                      
   Begin
      Op[1] := ' - ';
      DoOp(Answer, Final, 1, Op[1], OpVar[2]);
   End
   Else If Var1 = 3 Then
   Begin
      Op[1] := ' * ';
      DoOp(Answer, Final, 1, Op[1], OpVar[2]);
   End
   Else If Var1 = 4 Then
   Begin
      Op[1] := ' / ';
      DoOp(Answer, Final, 1, Op[1], OpVar[2]);
   End;
   { Set group of (xx)xx }
   Grp[2] := Answer;
   {==================================================}
   If Var2 = 1 Then
   Begin
      Op[2] := ' + ';
      DoOp(Answer, Final, 1, Op[2], OpVar[3]);
      Grp[3] := OpVar[2] + OpVar[3]; { x(xx)x }
      Grp[5] := Grp[2] + OpVar[3];   { (xxx)x }
   End
   Else If Var2 = 2 Then                      
   Begin
      Op[2] := ' - ';
      DoOp(Answer, Final, 1, Op[2], OpVar[3]);
      Grp[3] := OpVar[2] - OpVar[3];
      Grp[5] := Grp[2] - OpVar[3];
   End
   Else If Var2 = 3 Then
   Begin
      Op[2] := ' * ';
      DoOp(Answer, Final, 1, Op[2], OpVar[3]);
      Grp[3] := OpVar[2] * OpVar[3];
      Grp[5] := Grp[2] * OpVar[3];
   End
   Else If Var2 = 4 Then
   Begin
      Op[2] := ' / ';
      DoOp(Answer, Final, 1, Op[2], OpVar[3]);
      Grp[3] := OpVar[2] / OpVar[3];
      Grp[5] := Grp[2] / OpVar[3];
   End;
   {=================================================}
   If Var3 = 1 Then
   Begin
      Op[3] := ' + ';
      DoOp(Answer, Final, 1, Op[3], OpVar[4]);
      Grp[4] := OpVar[3] + OpVar[4];
      Grp[6] := Grp[3] + OpVar[4];
   End
   Else If Var3 = 2 Then                      
   Begin
      Op[3] := ' - ';
      DoOp(Answer, Final, 1, Op[3], OpVar[4]);
      Grp[4] := OpVar[3] - OpVar[4];
      Grp[6] := Grp[3] - OpVar[4];
   End
   Else If Var3 = 3 Then
   Begin
      Op[3] := ' * ';
      DoOp(Answer, Final, 1, Op[3], OpVar[4]);
      Grp[4] := OpVar[3] * OpVar[4];
      Grp[6] := Grp[3] * OpVar[4];
   End
   Else If Var3 = 4 Then
   Begin
      Op[3] := ' / ';
      DoOp(Answer, Final, 1, Op[3], OpVar[4]);
      Grp[4] := OpVar[3] / OpVar[4];
      Grp[6] := Grp[3] / OpVar[4];
   End;

   Final := 0;
   If Var4 = 1 Then
    Begin
      Final := Answer;
      Group := 'X X X X';
    End
   Else If Var4 = 2 Then
    Begin
      DoOp(Answer, Final, 2, ' + ', Grp[2]);
      DoOp(Answer, Final, 2, Op[2], OpVar[3]);
      DoOp(Answer, Final, 2, Op[3], OpVar[4]);
      Group := '(X X) X X';
    End
   Else If Var4 = 3 Then
    Begin
      DoOp(Answer, Final, 2, ' + ', OpVar[1]);
      DoOp(Answer, Final, 2, Op[1], Grp[3]);
      DoOp(Answer, Final, 2, Op[3], OpVar[4]);
      Group := 'X (X X) X';
    End
   Else If Var4 = 4 Then
    Begin
      DoOp(Answer, Final, 2, ' + ', OpVar[1]);
      DoOp(Answer, Final, 2, Op[1], OpVar[2]);
      DoOp(Answer, Final, 2, Op[2], Grp[4]);
      Group := 'X X (X X)';
    End
   Else If Var4 = 5 Then
    Begin
      DoOp(Answer, Final, 2, ' + ', Grp[2]);
      DoOp(Answer, Final, 2, Op[2], Grp[4]);
      Group := '(X X) (X X)';
    End
   Else If Var4 = 6 Then
    Begin
      DoOp(Answer, Final, 2, ' + ', Grp[5]);
      DoOp(Answer, Final, 2, Op[3], OpVar[4]);
      Group := '(X X X) X';
    End
   Else If Var4 = 7 Then
    Begin
      DoOp(Answer, Final, 2, ' + ', OpVar[1]);
      DoOp(Answer, Final, 2, Op[1], Grp[6]);
      Group := 'X (X X X)';
    End;

   If Final = 24
   Then 
   Begin
      Writeln;
      Write (OpVar[1], Op[1], OpVar[2], Op[2], OpVar[3], Op[3], OpVar[4]);
      Write (' = ', Final:1:2);
      WriteLn (' :: Solution found');
      WriteLn (Group);
      Check := 1;
   End
   Else 
      Check := 0;
End;

{SUB PROC OPERATE : CHECK END--------------------------------------------}

Begin    { Operate }
   If Num = 1 Then
      For A := 1 To 4 Do
         OpVar[A] := IVar[A]
   Else If Num = 2 Then
      For A := 1 To 4 Do
         OpVar[A] := TVar[A];

   For A := 1 To 4 Do
   Begin
      For B := 1 To 4 Do
      Begin
         For C := 1 To 4 Do
         Begin
            For D := 1 To 7 Do
            Begin
               If Check(A, B, C, D) = 1 Then
               Begin
                  Operate := 1;
                  Exit;
               End;
            End;
         End;
      End;
   End;

End;       { Operate }

{------------------------------------------------------------------------}

Procedure Main;

Begin { Main }
     ClrScr;
     Writeln ('Twenty-Four Solver');
     Writeln ('===================');
      For A := 1 To 4 Do
      Begin
         Repeat
            Write ('Number ', A, ': ');
            Readln (OVar[A]);
         Until (OVar[A] > 0);
      End;

      Found := 0;
      For A := 1 To 4 Do
      Begin
         If Found = 0 Then
         Begin
            For Count := 1 To 4 Do IVar[Count] := OVar[Count];

            Temp := IVar[A];
            IVar[A] := IVar[1];
            IVar[1] := Temp;
            
            For B := 2 To 4 Do
            Begin
               If Found = 0 Then
               Begin
                  Temp := IVar[2];
                  IVar[2] := IVar[B];
                  IVar[B] := Temp;
                  For Count := 1 To 4 Do TVar[Count] := IVar[Count];

                  If (Operate(1) = 1) Then Exit;

                  Temp := TVar[3];
                  TVar[3] := TVar[4];
                  TVar[4] := Temp;

                  If (Operate(2) = 1) Then Exit;
               End;
            End;
         End;
      End;
      Writeln;
      Writeln ('# No Solution #');
End;  {  Main  }

{========================================================================}

Begin
   TextBackGround(0);
   TextColor(15);
   ClrScr;
   Writeln ('                               Twenty-Four Solver');
   Writeln ('                   =======================================');
   Writeln ('                    This program solves any problem from');
   Writeln ('                    the game of thinking, ''Twenty-Four''.');
   Writeln ('                   =======================================');
   Writeln ('                                  By John Phan');
   Readln;

   YesNo := 'Y';
   While (YesNo = 'Y') Or (YesNo = 'y') Do
    Begin { Loop }
      Main;
      Writeln;
      Write ('Solve Another? (Y/N) ');
      Readln (YesNo);
    End;  { Loop }
End.

