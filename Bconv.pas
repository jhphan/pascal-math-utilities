Program BaseConverter;
{$N+}
Uses
   Crt;

Var
   Check,
   Base,
   BaseTo   : Integer;
   DoAnother: Char;
   Final,
   Number   : Comp;
   NewNumber: Array[1 .. 20] of Comp;
{=========================================================================}
Function Exponent (B , E: Integer) : Real;
Var
   Num  : comp;
   Count: Integer;

Begin
   Num := 1;
   For Count := 1 To E Do
       Num := Num * B;
   Exponent := Num;
End;
{=========================================================================}
Function ConvertDown (Number : Comp; Base, BaseTo : Integer) :Comp;
Var
   Count1,
   Count     : Integer;
   Answer,
   NumberLeft: Comp;

Begin
   Count := 1;
   NumberLeft := Number;
   Repeat
      NewNumber[Count] := Round(NumberLeft) Mod BaseTo;
      NumberLeft := Round(NumberLeft) Div BaseTo;
      Count := Count + 1;
   Until NumberLeft < BaseTo;
   NewNumber[Count] := (NumberLeft);

   Answer := 0;
   For Count1 := Count DownTo 1 Do
   Begin
      Answer := Answer + NewNumber[Count1] * Exponent(10, Count1 - 1);
   End;
   ConvertDown := Answer;
End;
{=========================================================================}
Function ConvertUp (Number: Comp; Base: Integer) : Comp;
Var
   Count1,
   Count     : Integer;
   Digit,
   Answer,
   NumberLeft: Comp;

Begin
   Count := -1;
   Repeat
      Count := Count + 1;
   Until Exponent(10, Count) > Number;

   For Count1 := 1 To Count Do
   Begin
      Digit := Round(Number) Mod 10;
      Number := (Number - Digit) / 10;
      NewNumber[Count1] := Digit * Exponent(Base, Count1 - 1);
   End;

   Answer := 0;
   For Count := Count1 DownTo 1 Do
   Begin
      Answer := Answer + NewNumber[Count];
   End;
   ConvertUp := Answer;
End;
{=========================================================================}
Function CheckNumber : Integer;
Var
   Count1,
   Digit,
   Check,
   Count :Integer;
   num: comp;
   ChNumber : Array[1 .. 20] Of Comp;

Begin
   Count := -1;
   num := number;
   Repeat
      Count := Count + 1;
   Until Exponent(10, Count) > num;

   For Count1 := 1 To Count Do
   Begin
      Digit := Round(Num) Mod 10;
      Num := (Num - Digit) / 10;
      ChNumber[Count1] := Digit;
   End;

   For Count1 := 1 To Count Do
   Begin
      If ChNumber[Count1] >= Base Then
      Begin
         Check := 1;
         Exit;
      End;
   End;
   If Check = 1 Then
   Begin
      CheckNumber := 1;
      Exit;
   End;
   CheckNumber := 0;
End;
{=========================================================================}
Begin
   
   TextBackGround(0);
   TextColor(15);
   ClrScr;
   Writeln ('                                Base Converter');
   Writeln ('             -===================================================-');
   Writeln ('              Created by: Jack Bewley, Phillip Key, And John Phan');
   Writeln ('             -===================================================-');
   Writeln ('              This program converts numbers in bases 2 through 10');
   Readln;
   ClrScr;

   DoAnother := 'Y';
   While (DoAnother = 'y') Or (DoAnother = 'Y') Do
   Begin
      ClrScr;
      WriteLn;
      Repeat
         Write ('Enter the base of the number to convert (2-10): ');
         Readln (Base);
      Until (Base <= 10) And (Base >= 2);
      Repeat
         Write ('Enter the base ', Base, ' number: ');
         ReadLn (Number);
      Until CheckNumber = 0;
      Repeat
         Write ('Convert to what base? ');
         ReadLn (BaseTo);
      Until (BaseTo <= 10) And (BaseTo >= 2);

      Final := 0;

      If Base = BaseTo Then
      Begin
         Writeln;
         Writeln ('You''re trying to convert to the same base, STUPID!!');
      End
      Else If Base > BaseTo Then
         Final := ConvertDown(Number, Base, BaseTo)

      Else If Base < BaseTo Then
         Final := ConvertDown(ConvertUp(Number, Base), 10, BaseTo);

      WriteLn;
      If Final > 0 Then
         WriteLn (Number:1:0, ' base ', Base, ' = ', Final:1:0, ' base ', BaseTo);

      WriteLn;
      Write ('Solve another? (Y/N) ');
      ReadLn (DoAnother);
   End;

End.