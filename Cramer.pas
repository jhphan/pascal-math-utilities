program Cramer;

uses
    Crt,
    Graph;

Var
    MM :array [1 .. 4, 1 .. 5] of Real;
    MCO :array [1 .. 4, 1 .. 4] of Real;
    MA :array [1 .. 4, 1 .. 4] of Real;
    MB :array [1 .. 4, 1 .. 4] of Real;
    MC :array [1 .. 4, 1 .. 4] of Real;
    MD :array [1 .. 4, 1 .. 4] of Real;
    Opt,
    YesNo :Char;

    CO, CO1, CO2, CO3, CO4,
    A, A1, A2, A3, A4,
    B, B1, B2, B3, B4,
    C, C1, C2, C3, C4,
    D, D1, D2, D3, D4       :Real;

    SA, SB, SC, SD          :Real;

    NumVars :Integer;

{---------------------------------------------------------------------------}
procedure GetInput;

Var
    Count,
    Count1 :Integer;
    Varc   :Char;

Begin

    ClrScr;
    Writeln ('Enter the values in standard form: A + B + C + D = E');
    Writeln ('====================================================');
    For Count1 := 1 to NumVars Do
    Begin
        Writeln;
        Writeln ('Equation ', Count1);
        Writeln;

        For Count := 1 to Numvars Do
        Begin
            IF (Count = 1) THEN Varc := 'A'
            ELSE If (Count = 2) Then Varc := 'B'
            Else If (Count = 3) Then Varc := 'C'
            Else If (Count = 4) Then Varc := 'D';
            Write ('Enter ', Varc, ' coefficient: ');
            Readln (MM[Count1, Count]);

            IF (Count = NumVars) THEN
            Begin
                Write ('Enter constant: ');
                Readln (MM[Count1, 5]);
            End;
        End;
    End;
End;
{---------------------------------------------------------------------------}
Procedure ShowInput;

Var
    Count :Integer;
    
Begin
    ClrScr;

    Writeln ('Main matrix:');
    Writeln;
    For Count := 1 to NumVars Do
    Begin
        Write (MM[Count, 1]:10:2);
        Write (MM[Count, 2]:10:2);
        If (NumVars >= 3) Then Write (MM[Count, 3]:10:2);
        If (NumVars >= 4) Then Write (MM[Count, 4]:10:2);
        Writeln (MM[Count, 5]:10:2);
    End;
    Writeln;
    Readln;
End;
{---------------------------------------------------------------------------}
Procedure SetDet;

Var
    Count :Integer;

Begin
     For Count := 1 to NumVars Do
     Begin
        {Set Determinant of Coefficients}
        MCO[Count, 1] := MM[Count, 1];
        MCO[Count, 2] := MM[Count, 2];
        If (NumVars >= 3) Then MCO[Count, 3] := MM[Count, 3];
        If (NumVars >= 4) Then MCO[Count, 4] := MM[Count, 4];

        {Set Determinant of A}
        MA[Count, 1] := MM[Count, 5];
        MA[Count, 2] := MM[Count, 2];
        If (NumVars >= 3) Then MA[Count, 3] := MM[Count, 3];
        If (NumVars >= 4) Then MA[Count, 4] := MM[Count, 4];

        {Set Determinant of B}
        MB[Count, 1] := MM[Count, 1];
        MB[Count, 2] := MM[Count, 5];
        If (NumVars >= 3) Then MB[Count, 3] := MM[Count, 3];
        If (NumVars >= 4) Then MB[Count, 4] := MM[Count, 4];

        If (NumVars >= 3) Then
        Begin
           {Set Determinant of C}
           MC[Count, 1] := MM[Count, 1];
           MC[Count, 2] := MM[Count, 2];
           MC[Count, 3] := MM[Count, 5];
           If (NumVars >= 4) Then MC[Count, 4] := MM[Count, 4];
        End;

        If (NumVars >= 4) Then
        Begin
           {Set Determinant of D}
           MD[Count, 1] := MM[Count, 1];
           MD[Count, 2] := MM[Count, 2];
           MD[Count, 3] := MM[Count, 3];
           MD[Count, 4] := MM[Count, 5];
        End;
     End;
End;
{---------------------------------------------------------------------------}
Procedure EvalDet;

Begin
    IF (NumVars = 2) THEN
    Begin
        CO := (MCO[1, 1] * MCO[2, 2]) - (MCO[2, 1] * MCO[1, 2]);
        A := (MA[1, 1] * MA[2, 2]) - (MA[2, 1] * MA[1, 2]);
        B := (MB[1, 1] * MB[2, 2]) - (MB[2, 1] * MB[1, 2]);
    END
    Else If (NumVars = 3) Then
    Begin
        CO := (MCO[1, 1] * ((MCO[2, 2] * MCO[3, 3]) - (MCO[3, 2] * MCO[2, 3])));
        CO := CO - (MCO[1, 2] * ((MCO[2, 1] * MCO[3, 3]) - (MCO[3, 1] * MCO[2, 3])));
        CO := CO + (MCO[1, 3] * ((MCO[2, 1] * MCO[3, 2]) - (MCO[3, 1] * MCO[2, 2])));

        A := (MA[1, 1] * ((MA[2, 2] * MA[3, 3]) - (MA[3, 2] * MA[2, 3])));
        A := A - (MA[1, 2] * ((MA[2, 1] * MA[3, 3]) - (MA[3, 1] * MA[2, 3])));
        A := A + (MA[1, 3] * ((MA[2, 1] * MA[3, 2]) - (MA[3, 1] * MA[2, 2])));

        B := (MB[1, 1] * ((MB[2, 2] * MB[3, 3]) - (MB[3, 2] * MB[2, 3])));
        B := B - (MB[1, 2] * ((MB[2, 1] * MB[3, 3]) - (MB[3, 1] * MB[2, 3])));
        B := B + (MB[1, 3] * ((MB[2, 1] * MB[3, 2]) - (MB[3, 1] * MB[2, 2])));

        C := (MC[1, 1] * ((MC[2, 2] * MC[3, 3]) - (MC[3, 2] * MC[2, 3])));
        C := C - (MC[1, 2] * ((MC[2, 1] * MC[3, 3]) - (MC[3, 1] * MC[2, 3])));
        C := C + (MC[1, 3] * ((MC[2, 1] * MC[3, 2]) - (MC[3, 1] * MC[2, 2])));
    END
    Else if (NumVars = 4) Then
    Begin
        {Evaluate Determinate of Coefficients}
        CO1 := (MCO[2, 2] * ((MCO[3, 3] * MCO[4, 4]) - (MCO[4, 3] * MCO[3, 4])));
        CO1 := CO1 - (MCO[2, 3] * ((MCO[3, 2] * MCO[4, 4]) - (MCO[4, 2] * MCO[3, 4])));
        CO1 := CO1 + (MCO[2, 4] * ((MCO[3, 2] * MCO[4, 3]) - (MCO[4, 2] * MCO[3, 3])));
        CO1 := CO1 * MCO[1, 1];

        CO2 := (MCO[2, 1] * ((MCO[3, 3] * MCO[4, 4]) - (MCO[4, 3] * MCO[3, 4])));
        CO2 := CO2 - (MCO[2, 3] * ((MCO[3, 1] * MCO[4, 4]) - (MCO[4, 1] * MCO[3, 4])));
        CO2 := CO2 + (MCO[2, 4] * ((MCO[3, 1] * MCO[4, 3]) - (MCO[4, 1] * MCO[3, 3])));
        CO2 := CO2 * MCO[1, 2];

        CO3 := (MCO[2, 1] * ((MCO[3, 2] * MCO[4, 4]) - (MCO[4, 2] * MCO[3, 4])));
        CO3 := CO3 - (MCO[2, 2] * ((MCO[3, 1] * MCO[4, 4]) - (MCO[4, 1] * MCO[3, 4])));
        CO3 := CO3 + (MCO[2, 4] * ((MCO[3, 1] * MCO[4, 2]) - (MCO[4, 1] * MCO[3, 2])));
        CO3 := CO3 * MCO[1, 3];

        CO4 := (MCO[2, 1] * ((MCO[3, 2] * MCO[4, 3]) - (MCO[4, 2] * MCO[3, 3])));
        CO4 := CO4 - (MCO[2, 2] * ((MCO[3, 1] * MCO[4, 3]) - (MCO[4, 1] * MCO[3, 3])));
        CO4 := CO4 + (MCO[2, 3] * ((MCO[3, 1] * MCO[4, 2]) - (MCO[4, 1] * MCO[3, 2])));
        CO4 := CO4 * MCO[1, 4];

        CO := CO1 - CO2 + CO3 - CO4;
        {Evaluate Determinant of A}
        A1 := (MA[2, 2] * ((MA[3, 3] * MA[4, 4]) - (MA[4, 3] * MA[3, 4])));
        A1 := A1 - (MA[2, 3] * ((MA[3, 2] * MA[4, 4]) - (MA[4, 2] * MA[3, 4])));
        A1 := A1 + (MA[2, 4] * ((MA[3, 2] * MA[4, 3]) - (MA[4, 2] * MA[3, 3])));
        A1 := A1 * MA[1, 1];

        A2 := (MA[2, 1] * ((MA[3, 3] * MA[4, 4]) - (MA[4, 3] * MA[3, 4])));
        A2 := A2 - (MA[2, 3] * ((MA[3, 1] * MA[4, 4]) - (MA[4, 1] * MA[3, 4])));
        A2 := A2 + (MA[2, 4] * ((MA[3, 1] * MA[4, 3]) - (MA[4, 1] * MA[3, 3])));
        A2 := A2 * MA[1, 2];

        A3 := (MA[2, 1] * ((MA[3, 2] * MA[4, 4]) - (MA[4, 2] * MA[3, 4])));
        A3 := A3 - (MA[2, 2] * ((MA[3, 1] * MA[4, 4]) - (MA[4, 1] * MA[3, 4])));
        A3 := A3 + (MA[2, 4] * ((MA[3, 1] * MA[4, 2]) - (MA[4, 1] * MA[3, 2])));
        A3 := A3 * MA[1, 3];

        A4 := (MA[2, 1] * ((MA[3, 2] * MA[4, 3]) - (MA[4, 2] * MA[3, 3])));
        A4 := A4 - (MA[2, 2] * ((MA[3, 1] * MA[4, 3]) - (MA[4, 1] * MA[3, 3])));
        A4 := A4 + (MA[2, 3] * ((MA[3, 1] * MA[4, 2]) - (MA[4, 1] * MA[3, 2])));
        A4 := A4 * MA[1, 4];

        A := A1 - A2 + A3 - A4;
        {Evaluate Determinate of B}
        B1 := (MB[2, 2] * ((MB[3, 3] * MB[4, 4]) - (MB[4, 3] * MB[3, 4])));
        B1 := B1 - (MB[2, 3] * ((MB[3, 2] * MB[4, 4]) - (MB[4, 2] * MB[3, 4])));
        B1 := B1 + (MB[2, 4] * ((MB[3, 2] * MB[4, 3]) - (MB[4, 2] * MB[3, 3])));
        B1 := B1 * MB[1, 1];

        B2 := (MB[2, 1] * ((MB[3, 3] * MB[4, 4]) - (MB[4, 3] * MB[3, 4])));
        B2 := B2 - (MB[2, 3] * ((MB[3, 1] * MB[4, 4]) - (MB[4, 1] * MB[3, 4])));
        B2 := B2 + (MB[2, 4] * ((MB[3, 1] * MB[4, 3]) - (MB[4, 1] * MB[3, 3])));
        B2 := B2 * MB[1, 2];

        B3 := (MB[2, 1] * ((MB[3, 2] * MB[4, 4]) - (MB[4, 2] * MB[3, 4])));
        B3 := B3 - (MB[2, 2] * ((MB[3, 1] * MB[4, 4]) - (MB[4, 1] * MB[3, 4])));
        B3 := B3 + (MB[2, 4] * ((MB[3, 1] * MB[4, 2]) - (MB[4, 1] * MB[3, 2])));
        B3 := B3 * MB[1, 3];

        B4 := (MB[2, 1] * ((MB[3, 2] * MB[4, 3]) - (MB[4, 2] * MB[3, 3])));
        B4 := B4 - (MB[2, 2] * ((MB[3, 1] * MB[4, 3]) - (MB[4, 1] * MB[3, 3])));
        B4 := B4 + (MB[2, 3] * ((MB[3, 1] * MB[4, 2]) - (MB[4, 1] * MB[3, 2])));
        B4 := B4 * MB[1, 4];

        B := B1 - B2 + B3 - B4;
        {Evaluate Determinant of C}
        C1 := (MC[2, 2] * ((MC[3, 3] * MC[4, 4]) - (MC[4, 3] * MC[3, 4])));
        C1 := C1 - (MC[2, 3] * ((MC[3, 2] * MC[4, 4]) - (MC[4, 2] * MC[3, 4])));
        C1 := C1 + (MC[2, 4] * ((MC[3, 2] * MC[4, 3]) - (MC[4, 2] * MC[3, 3])));
        C1 := C1 * MC[1, 1];

        C2 := (MC[2, 1] * ((MC[3, 3] * MC[4, 4]) - (MC[4, 3] * MC[3, 4])));
        C2 := C2 - (MC[2, 3] * ((MC[3, 1] * MC[4, 4]) - (MC[4, 1] * MC[3, 4])));
        C2 := C2 + (MC[2, 4] * ((MC[3, 1] * MC[4, 3]) - (MC[4, 1] * MC[3, 3])));
        C2 := C2 * MC[1, 2];

        C3 := (MC[2, 1] * ((MC[3, 2] * MC[4, 4]) - (MC[4, 2] * MC[3, 4])));
        C3 := C3 - (MC[2, 2] * ((MC[3, 1] * MC[4, 4]) - (MC[4, 1] * MC[3, 4])));
        C3 := C3 + (MC[2, 4] * ((MC[3, 1] * MC[4, 2]) - (MC[4, 1] * MC[3, 2])));
        C3 := C3 * MC[1, 3];

        C4 := (MC[2, 1] * ((MC[3, 2] * MC[4, 3]) - (MC[4, 2] * MC[3, 3])));
        C4 := C4 - (MC[2, 2] * ((MC[3, 1] * MC[4, 3]) - (MC[4, 1] * MC[3, 3])));
        C4 := C4 + (MC[2, 3] * ((MC[3, 1] * MC[4, 2]) - (MC[4, 1] * MC[3, 2])));
        C4 := C4 * MC[1, 4];

        C := C1 - C2 + C3 - C4;
        {Evaluate Determinant of D}
        D1 := (MD[2, 2] * ((MD[3, 3] * MD[4, 4]) - (MD[4, 3] * MD[3, 4])));
        D1 := D1 - (MD[2, 3] * ((MD[3, 2] * MD[4, 4]) - (MD[4, 2] * MD[3, 4])));
        D1 := D1 + (MD[2, 4] * ((MD[3, 2] * MD[4, 3]) - (MD[4, 2] * MD[3, 3])));
        D1 := D1 * MD[1, 1];

        D2 := (MD[2, 1] * ((MD[3, 3] * MD[4, 4]) - (MD[4, 3] * MD[3, 4])));
        D2 := D2 - (MD[2, 3] * ((MD[3, 1] * MD[4, 4]) - (MD[4, 1] * MD[3, 4])));
        D2 := D2 + (MD[2, 4] * ((MD[3, 1] * MD[4, 3]) - (MD[4, 1] * MD[3, 3])));
        D2 := D2 * MD[1, 2];

        D3 := (MD[2, 1] * ((MD[3, 2] * MD[4, 4]) - (MD[4, 2] * MD[3, 4])));
        D3 := D3 - (MD[2, 2] * ((MD[3, 1] * MD[4, 4]) - (MD[4, 1] * MD[3, 4])));
        D3 := D3 + (MD[2, 4] * ((MD[3, 1] * MD[4, 2]) - (MD[4, 1] * MD[3, 2])));
        D3 := D3 * MD[1, 3];

        D4 := (MD[2, 1] * ((MD[3, 2] * MD[4, 3]) - (MD[4, 2] * MD[3, 3])));
        D4 := D4 - (MD[2, 2] * ((MD[3, 1] * MD[4, 3]) - (MD[4, 1] * MD[3, 3])));
        D4 := D4 + (MD[2, 3] * ((MD[3, 1] * MD[4, 2]) - (MD[4, 1] * MD[3, 2])));
        D4 := D4 * MD[1, 4];

        D := D1 - D2 + D3 - D4;
    End;
End;
{---------------------------------------------------------------------------}
Procedure DisplayDet;

Var
   Count :Integer;

Begin

     Writeln ('Determinant of coefficients:');
     Writeln;
     For Count := 1 to NumVars do
     Begin
          Write (MCO[Count, 1]:10:2);
          Write (MCO[Count, 2]:10:2);
          If (NumVars >= 3) Then Write (MCO[Count, 3]:10:2);
          If (NumVars >= 4) Then Write (MCO[Count, 4]:10:2);
          Writeln;
     End;
     Writeln (' = ', CO:10:2);
     Readln;

     Writeln ('Determinant of A');
     Writeln;
     For Count := 1 to NumVars do
     Begin
          Write (MA[Count, 1]:10:2);
          Write (MA[Count, 2]:10:2);
          If (NumVars >= 3) Then Write (MA[Count, 3]:10:2);
          If (NumVars >= 4) Then Write (MA[Count, 4]:10:2);
          Writeln;
     End;
     Writeln (' = ', A:10:2);
     Readln;

     Writeln ('Determinant of B');
     Writeln;
     For Count := 1 to NumVars do
     Begin
          Write (MB[Count, 1]:10:2);
          Write (MB[Count, 2]:10:2);
          If (NumVars >= 3) Then Write (MB[Count, 3]:10:2);
          If (NumVars >= 4) Then Write (MB[Count, 4]:10:2);
          Writeln;
     End;
     Writeln (' = ', B:10:2);
     Readln;

     If (NumVars >= 3) Then
     Begin
     Writeln ('Determinant of C');
     Writeln;
     For Count := 1 to NumVars do
     Begin
          Write (MC[Count, 1]:10:2);
          Write (MC[Count, 2]:10:2);
          If (NumVars >= 3) Then Write (MC[Count, 3]:10:2);
          If (NumVars >= 4) Then Write (MC[Count, 4]:10:2);
          Writeln;
     End;
     Writeln (' = ', C:10:2);
     Readln;
     End;

     If (NumVars >= 4) Then
     Begin
     Writeln ('Determinant of D');
     Writeln;
     For Count := 1 to NumVars do
     Begin
          Write (MD[Count, 1]:10:2);
          Write (MD[Count, 2]:10:2);
          If (NumVars >= 3) Then Write (MD[Count, 3]:10:2);
          If (NumVars >= 4) Then Write (MD[Count, 4]:10:2);
          Writeln;
     End;
     Writeln (' = ', D:10:2);
     Readln;
     End;

End;
{---------------------------------------------------------------------------}
Procedure FindSolution;

Begin
    SA := A/CO;
    SB := B/CO;
    If (NumVars >= 3) Then SC := C/CO;
    If (NumVars >= 4) Then SD := D/CO;
End;
{---------------------------------------------------------------------------}
Procedure DisplaySolution;

Begin
    ClrScr;
    Writeln ('Solution:');
    Writeln;
    Writeln ('A = ', SA:10:4);
    Writeln ('B = ', SB:10:4);
    If (NumVars >= 3) Then Writeln ('C = ', SC:10:4);
    If (NumVars >= 4) Then Writeln ('D = ', SD:10:4);
    Writeln;
    Readln;
End;
{---------------------------------------------------------------------------}
Begin

    Clrscr;
    Writeln ('Cramer''s Rule':45);
    Writeln ('===============================================':62);
    Writeln ('This program uses Cramer''s rule to solve':58);
    Writeln ('systems of equations with 2, 3, or 4 variables.':62);
    Writeln ('===============================================':62);
    Writeln ('By John Phan':44);
    readln;

    Repeat
    Repeat
        clrscr;
        Write ('Enter number of variables (2-4, 1 to quit): ');
        readln(Opt);
    Until (Opt = '1') or (Opt = '2') or (Opt = '3') or (Opt = '4');

    If (Opt = '1') Then Halt
        ELSE
    If (Opt = '2') Then NumVars := 2
        ELSE
    If (Opt = '3') then NumVars := 3
        ELSE
    If (Opt = '4') then NumVars := 4;

       GetInput;
       ShowInput;
       SetDet;
       EvalDet;

       If (CO = 0) Then
       Begin
          Writeln ('Error: Division by zero!');
          Readln;
       End
       Else
       Begin
          DisplayDet;
          FindSolution;
          DisplaySolution;
       End;

       Write ('Solve another? (Y/N)');
       Readln (YesNo);
       If (YesNo = 'Y') Or (YesNo = 'y') Then
          Else Halt;

    Until (1 <> 1);

    Readln;

end.

